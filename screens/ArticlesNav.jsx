import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { ArticlesMainScreen } from './ArticlesMainScreen';
import { FullPostScreen } from './FullPostScreen';

const Stack = createStackNavigator();

export const Navigation = () => {
    return (
    <NavigationContainer>
        <Stack.Navigator>
            <Stack.Screen name="Articles" component={ArticlesMainScreen} options={{title: 'Статьи'}} />
            <Stack.Screen name="FullPost" component={FullPostScreen} options={{title: 'Статья'}} />
        </Stack.Navigator>
    </NavigationContainer>
    );
} 