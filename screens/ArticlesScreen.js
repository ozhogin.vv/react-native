import * as React from 'react';
import { View, FlatList, ActivityIndicator, Text, TouchableOpacity} from 'react-native';
import { Post } from '../components/Post';
import axios from 'axios';

export default function ArticlesScreen({ navigation }) {
    const [articles, setArticles] = React.useState();
    const [isLoading, setIsLoading] = React.useState(true);

    const fetchPosts = () => {
        setIsLoading(true);
        axios.get('https://66782c040bd45250561dbe9e.mockapi.io/api/v1/articles')
        .then(({ data }) => {
            setArticles(data);
        }).catch(err => {
            console.log(err);
            alert('Ошибка при получении статей');
        }).finally(() => {
            setIsLoading(false);
        })
    }

    React.useEffect(fetchPosts, []);

    if (isLoading) {
        return <View style={
            {
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
            }
        }>
            <ActivityIndicator size="large"/>
            <Text>Загрузка статей...</Text>
        </View>
    }

    if (!isLoading) {
        return (
            <View>
                <FlatList
                    data={articles}
                    renderItem={(article) => (
                        <TouchableOpacity>
                            <Post 
                                title={article.item.title}
                                imageUrl={article.item.imageUrl}
                                createdAt={article.item.createdAt}
                            />
                        </TouchableOpacity>
                    )}
                />
            </View>
        );
    }
}