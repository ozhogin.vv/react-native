
// Импортируем необходимые компоненты из 'react' и 'react-native'
import React from 'react';
import { View, Text, StyleSheet, Platform } from 'react-native';

// Создаем функциональный компонент AndroidComponent
const HomeScreen = () => {
  if (Platform.Version === 25) {
    console.log('Running on Nougat')
  }
  // Проверяем, используется ли платформа Android
  if (Platform.OS === 'android') {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Android component</Text>
      </View>
    );
  } else {
    // Если компонент используется не на Android, ничего не отображаем
    return null;
  }
};

// Определяем стили для компонента
const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: 'lightgreen',
    alignItems: 'center',
  },
  text: {
    fontSize: 20,
    color: 'darkgreen',
  },
});

// Экспортируем компонент для использования в других частях приложения
export default HomeScreen;