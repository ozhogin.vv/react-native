// Импортируем необходимые компоненты из react-native
import { useState, useEffect } from 'react';
import { StyleSheet, SafeAreaView, Text, Button } from 'react-native';

export default function Counter() {
    const storeData = async (value) => {
        try {
          await AsyncStorage.setItem('1', value)
        } catch (e) {
          // saving error
        }
      }
      
      const getData = async () => {
        try {
          const value = await AsyncStorage.getItem('1')
          if(value !== null) {
            // value previously stored
          }
        } catch(e) {
          // error reading value
        }
      }

    useEffect(() => {
        console.log('CounterDidMount')
        return () => {
            console.log('CounterDidUnmount')
        }
    }, [])

    useEffect(() => {
        console.log('Render happend')
        storeData(counter)
    })

    const [counter, setCounter] = useState(0);

    const incrementCounter = () => {
        setCounter(counter + 1);
    };

    return (
        <SafeAreaView style={styles.container}>
            <Button title={'+'} onPress={incrementCounter} />
            <Text>{counter}</Text>
        </SafeAreaView>
    );
}

// Определение стилей с помощью StyleSheet.create
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
});
