import React from 'react';
import { render, waitFor } from '@testing-library/react-native';
import ArticlesScreen from '../screens/ArticlesScreen';
import axios from 'axios';
import { ActivityIndicator } from 'react-native';

// Мокаем запросы к API
jest.mock('axios');

describe('ArticlesScreen', () => {
  it('отображает индикатор загрузки перед загрузкой статей', () => {
    // Мокаем состояние загрузки
    axios.get.mockResolvedValueOnce({ data: [] });
    const { getByType } = render(<ArticlesScreen />);
    expect(getByType(ActivityIndicator)).toBeTruthy();
  });

  it('отображает список статей после загрузки данных', async () => {
    // Мокаем полученные данные
    const articles = [
      { id: '1', title: 'Статья 1', imageUrl: 'image-url-1', createdAt: 'date-1' },
      // Добавьте столько объектов, сколько необходимо для теста
    ];
    axios.get.mockResolvedValueOnce({ data: articles });

    const { getByText, queryByType } = render(<ArticlesScreen />);

    // Проверяем, что индикатор загрузки исчез после получения данных
    await waitFor(() => expect(queryByType(ActivityIndicator)).toBeFalsy());

    // Проверяем, что все заголовки статей отображаются
    articles.forEach(article => {
      expect(getByText(article.title)).toBeTruthy();
    });
  });
});