// Импортируем необходимые компоненты из react-native
import { StyleSheet, SafeAreaView, Button, Alert, Image, TextInput} from 'react-native';

// Функциональный компонент Main с параметром navigation для возможности перехода
// на экран 2 по нажатию кнопки
export default function Main({ navigation }) {
  // Функция showAlert для отображения всплывающего окна
  const showAlert = () => Alert.alert('Ozhogin alert', 'Ok or not ok', [
    {text: 'Ок', onPress: () => console.log('Ок')},
    {text: 'не ок', onPress: () => console.log('не ок')},
  ]);

  // Функция loadScreen для перехода на экран 2 при нажатии кнопки
  const loadScreen = () => {
    navigation.navigate('Screen2');
  }

  // Возвращаем разметку компонента
  return (
    // SafeAreaView отображает вложенный контент и автоматически применяет отступы
    <SafeAreaView style={styles.container}>
      {/* Кнопка для отображения всплывающего окна */}
      <Button title={'Show alert'} onPress={showAlert}/>
      {/* Кнопка для перехода на экран 2 */}
      <Button title={'Состояния'} onPress={loadScreen} />
      {/* Текстовое поле */}
      <TextInput style={styles.input}></TextInput>
      {/* Изображение */}
      <Image source={require('./assets/12.jpg')} style={styles.image} />
    </SafeAreaView>
  );
}

// Определение стилей с помощью StyleSheet.create
const styles = StyleSheet.create({
  // Стили для контейнера SafeAreaView
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  // Стили для изображения
  image: {
    width: 400,
    height: 250,
  },
  // Стили для текстового поля
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});
