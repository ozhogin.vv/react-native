// Импортируем необходимые компоненты из react-native
import { useState, useEffect } from 'react';
import { StyleSheet, SafeAreaView, Text, Button } from 'react-native';
import Counter from './Counter';

export default function StatesScreen() {
  const [toggler, setToggler] = useState(false);
  
  return (
    <SafeAreaView style={styles.container}>
      <Button title={'Show counter'} onPress={() => {setToggler(!toggler)}} />
      {
        toggler ?
        <Counter></Counter>
        : ''
      }
      
    </SafeAreaView>
  );
}

// Определение стилей с помощью StyleSheet.create
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
