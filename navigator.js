import React from 'react';

import StatesScreen from './screens/StatesScreen';
import Main from './Main';

// Импортируем createStackNavigator и NavigationContainer
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

// Создаем стек навигатора с помощью createStackNavigator
const Stack = createStackNavigator();

// Функциональный компонент Navigate
export default function Navigate() {
    // Возвращаем компонент NavigationContainer, который содержит стек навигатор
    return (
        <NavigationContainer>
            <Stack.Navigator>
                {/* Определяем экраны стека навигатора */}
                <Stack.Screen 
                    name='Главная' // Название экрана
                    component={Main} // Экран
                    options={{title: 'Главная (lab 2,3)'}} // Заголовок экрана
                />
                <Stack.Screen 
                    name='Screen2' // Название экрана
                    component={StatesScreen} // Экран
                    options={{title: 'Состояния'}} // Заголовок экрана
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
}
